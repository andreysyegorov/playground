package graph

import (
	"bytes"
	"fmt"
)

type Graph struct {
    Vertices []*Vertex
    Edges []*Edge
}

type Vertex struct {
    Value interface{}
}

type Edge struct {
    From *Vertex
    To *Vertex
    Weight float64
}

func (g Graph) String() string {
	var buf bytes.Buffer
	buf.WriteString("[")
	for i, v := range g.Vertices {
		if i != 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(v.String())
	}
	buf.WriteString("]")
	vertices := buf.String()
	buf = bytes.Buffer{}
	buf.WriteString("[")
	for i, e := range g.Edges {
		if i != 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(e.String())
	}
	buf.WriteString("]")
	edges := buf.String()
	return fmt.Sprintf("Graph\nVertices: %s\nEdges: %s", vertices, edges)
}

func (v *Vertex) String() string {
	return fmt.Sprintf("%s", v.Value)
}

func (e *Edge) String() string {
	return fmt.Sprintf("%s -> %s", e.From, e.To)
}

func (g *Graph) AddVertex(v *Vertex) {
	g.Vertices = append(g.Vertices, v)
}

func (g *Graph) AddEdge(from, to *Vertex) {
	g.Edges = append(g.Edges, &Edge{From: from, To: to})
}

func (g *Graph) AddBiEdge(a, b *Vertex) {
	g.Edges = append(g.Edges, &Edge{From: a, To: b}, &Edge{From: b, To: a})
}

func (g *Graph) AddWeightedBiEdge(a, b *Vertex, w float64) {
	g.Edges = append(g.Edges, &Edge{From: a, To: b, Weight: w}, &Edge{From: b, To: a, Weight: w})
}

func (g *Graph) GetVertex(value interface{}) *Vertex {
	for _, v := range g.Vertices {
		if v.Value == value {
			return v
		}
	}
	return nil
}

func (g *Graph) GetAdj(v *Vertex) []*Vertex {
	adj := make([]*Vertex, 0)
	for _, e := range g.Edges {
		if e.From == v {
			adj = append(adj, e.To)
		}
	}
	return adj
}

func (g *Graph) GetFromEdges(v *Vertex) []*Edge {
	adj := make([]*Edge, 0)
	for _, e := range g.Edges {
		if e.From == v {
			adj = append(adj, e)
		}
	}
	return adj
}
