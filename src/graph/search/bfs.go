package main

import (
    graph "graph/common"
    "math"
    "fmt"
)

func BFS(g graph.Graph, s *graph.Vertex) map[*graph.Vertex]uint64 {
    var white, grey, black uint8 = 1, 2, 3
    color := make(map[*graph.Vertex]uint8)
    d := make(map[*graph.Vertex]uint64)
    p := make(map[*graph.Vertex]*graph.Vertex)
    for _, u := range g.Vertices {
        if u == s {
            color[u] = grey
            d[u] = 0
        } else {
            color[u] = white
            d[u] = uint64(math.Inf(1))
        }
        p[u] = nil
    }
    Q := []*graph.Vertex{s}
    for len(Q) > 0 {
        u := Q[0]
        for _, v := range g.GetAdj(u) {
            if color[v] == white {
                color[v] = grey
                d[v] = d[u] + 1
                p[v] = u
                Q = append(Q, v)
            }
        }
        Q = Q[1:]
        color[u] = black
    }
    return d
}

func buildGraph() graph.Graph {
    var g graph.Graph
    a := graph.Vertex{"A"}
    b := graph.Vertex{"B"}
    c := graph.Vertex{"C"}
    d := graph.Vertex{"D"}
    e := graph.Vertex{"E"}
    f := graph.Vertex{"F"}
    j := graph.Vertex{"J"}
    m := graph.Vertex{"M"}
    n := graph.Vertex{"N"}
    g.Vertices = append(g.Vertices, &a, &b, &c, &d, &e, &f, &j, &m, &n)
    g.AddBiEdge(&a, &c)
    g.AddBiEdge(&b, &c)
    g.AddBiEdge(&c, &d)
    g.AddBiEdge(&d, &e)
    g.AddBiEdge(&e, &f)
    g.AddBiEdge(&b, &m)
    g.AddBiEdge(&m, &n)
    g.AddEdge(&f, &c)
    return g
}

func printDistances(d map[*graph.Vertex]uint64) {
    fmt.Println("Distances:")
    for u, sd := range d {
        if sd == uint64(math.Inf(1)) {
            fmt.Printf("%v - Infinity\n", u.Value)
        } else {
            fmt.Printf("%v - %d\n", u.Value, sd)
        }
    }
}

func main() {
    fmt.Println("Breadth-First Search on Graph")
    g := buildGraph()
    start := g.GetVertex("A")
    fmt.Println(g)
    fmt.Println("Initial Vertex:", start)
    d := BFS(g, start)
    printDistances(d)
}
