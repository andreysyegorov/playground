package main

import (
    graph "graph/common"
    "errors"
    "fmt"
    "math"
    "os"
)

type Point struct {
    X, Y float64
}

func AStar(g *graph.Graph, start, goal *graph.Vertex) (float64, []*graph.Vertex, error) {
    // set of already evaluated vertices
    closedSet := make(map[*graph.Vertex]bool)
    // set of currently discovered vertices
    openSet := make(map[*graph.Vertex]bool)
    openSet[start] = true
    // vertex from which the current one may be reached most efficiently
    cameFrom := make(map[*graph.Vertex]*graph.Vertex)
    // cost of getting from start vertex to that vertex
    gScore := make(map[*graph.Vertex]float64)
    for _, v := range g.Vertices {
        gScore[v] = float64(math.Inf(1))
    }
    gScore[start] = 0
    // total cost of getting from start vertex to the goal by passing that vertex (partly known, partly heuristic)
    fScore := make(map[*graph.Vertex]float64)
    fScore[start] = gScore[start] + heuristicCostEstimate(start, goal)

    for len(openSet) > 0 {
        current := lowestFScoreVertex(openSet, fScore)
        if current == goal {
            return gScore[current], reconstructPath(cameFrom, current), nil
        }
        delete(openSet, current)
        closedSet[current] = true

        for _, edge := range g.GetFromEdges(current) {
            neighbor := edge.To
            if closedSet[neighbor] {
                continue
            }
            tentativeGScore := gScore[current] + edge.Weight
            if !openSet[neighbor] {
                openSet[neighbor] = true
            } else if tentativeGScore >= gScore[neighbor] {
                continue
            }

            // current path is better than previous one - record it
            cameFrom[neighbor] = current
            gScore[neighbor] = tentativeGScore
            fScore[neighbor] = gScore[neighbor] + heuristicCostEstimate(neighbor, goal)
        }
    }
    return math.Inf(1), nil, errors.New(fmt.Sprintf("Path from %#v to %#v not found", start, goal))
}

func lowestFScoreVertex(openSet map[*graph.Vertex]bool, fScore map[*graph.Vertex]float64) *graph.Vertex {
    var v *graph.Vertex
    vFScore := float64(math.Inf(1))
    for u, _ := range openSet {
        if vFScore > fScore[u] {
            v, vFScore = u, fScore[u]
        }
    }
    return v
}

func heuristicCostEstimate(v1, v2 *graph.Vertex) float64 {
    return distBetweenPoints(v1.Value.(Point), v2.Value.(Point))
}

func distBetweenPoints(p1, p2 Point) float64 {
    return math.Sqrt(math.Pow(p2.X-p1.X, 2) + math.Pow(p2.Y-p1.Y, 2))
}

func reconstructPath(cameFrom map[*graph.Vertex]*graph.Vertex, current *graph.Vertex) []*graph.Vertex {
    path := append(make([]*graph.Vertex, 0, 1), current)
    for {
        current = cameFrom[current]
        if current == nil {
            break
        }
        path = append(path, current)
    }
    reverse(path)
    return path
}

func buildGraph() graph.Graph {
    var g graph.Graph
    v1 := graph.Vertex{Point{1, 1}}
    v2 := graph.Vertex{Point{1, 4}}
    v3 := graph.Vertex{Point{3, 5}}
    v4 := graph.Vertex{Point{5, 5}}
    v5 := graph.Vertex{Point{7, 5}}
    v6 := graph.Vertex{Point{7, 1}}
    v7 := graph.Vertex{Point{2, 2}}
    v8 := graph.Vertex{Point{2, 3}}
    v9 := graph.Vertex{Point{3, 3}}
    v10 := graph.Vertex{Point{3, 4}}
    v11 := graph.Vertex{Point{5, 4}}
    v12 := graph.Vertex{Point{6, 2}}
    v13 := graph.Vertex{Point{4, 2}}
    v14 := graph.Vertex{Point{5, 1}}
    v15 := graph.Vertex{Point{6, 1}}
    g.Vertices = append(g.Vertices, &v1, &v2, &v3, &v4, &v5, &v6, &v7, &v8, &v9, &v10, &v11, &v12, &v13, &v14, &v15)
    addEdge(&g, &v1, &v2)
    addEdge(&g, &v2, &v3)
    addEdge(&g, &v3, &v4)
    addEdge(&g, &v4, &v5)
    addEdge(&g, &v5, &v6)
    addEdge(&g, &v1, &v7)
    addEdge(&g, &v7, &v8)
    addEdge(&g, &v8, &v9)
    addEdge(&g, &v9, &v10)
    addEdge(&g, &v10, &v11)
    addEdge(&g, &v11, &v12)
    addEdge(&g, &v12, &v6)
    addEdge(&g, &v7, &v13)
    addEdge(&g, &v13, &v14)
    addEdge(&g, &v14, &v15)
    return g
}

func addEdge(g *graph.Graph, v1, v2 *graph.Vertex) {
    g.AddWeightedBiEdge(v1, v2, distBetweenPoints(v1.Value.(Point), v2.Value.(Point)))
}

func printGraph(g *graph.Graph) {
    fmt.Printf("Vertices: ")
    for i, v := range g.Vertices {
        if i > 0 {
            fmt.Printf(", ")
        }
        fmt.Printf(vertexToString(v))
    }
    fmt.Printf("\nEdges: ")
    for i, e := range g.Edges {
        if i > 0 {
            fmt.Printf(", ")
        }
        fmt.Printf("%v -> %v", vertexToString(e.From), vertexToString(e.To))
    }
    fmt.Println()
}

func printDistAndPath(dist float64, path []*graph.Vertex) {
    fmt.Printf("Distance: %.2f\n", dist)
    fmt.Printf("Path: ")
    for i, v := range path {
        if i > 0 {
            fmt.Printf(" -> ")
        }
        fmt.Printf(vertexToString(v))
    }
}

func vertexToString(v *graph.Vertex) string {
    return fmt.Sprintf("(%v, %v)", v.Value.(Point).X, v.Value.(Point).Y)
}

func reverse(a []*graph.Vertex) {
    for i := len(a)/2-1; i >= 0; i-- {
        opp := len(a)-1-i
        a[i], a[opp] = a[opp], a[i]
    }
}

func main() {
    fmt.Println("A* Search on Graph")
    g := buildGraph()
    start := g.GetVertex(Point{1, 1})
    goal := g.GetVertex(Point{7, 1})
    fmt.Println("Input")
    printGraph(&g)
    fmt.Printf("Start Vertex: %v\n", vertexToString(start))
    fmt.Printf("Goal Vertex: %v\n", vertexToString(goal))
    dist, path, err := AStar(&g, start, goal)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%v\n", err)
    } else {
        fmt.Println("Output")
        printDistAndPath(dist, path)
    }
}
