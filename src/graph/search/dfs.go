package main

import (
    graph "graph/common"
    "fmt"
)

const (
    white, grey, black uint8 = 1, 2, 3
)

func DFS(g *graph.Graph) []*graph.Vertex {
    color := make(map[*graph.Vertex]uint8)
    for _, v := range g.Vertices {
        color[v] = white
    }
    reached := make([]*graph.Vertex, 0)
    for _, v := range g.Vertices {
        if color[v] == white {
            reached = append(reached, v)
            r := vertexDFS(g, v, color)
            reached = append(reached, r...)
        }
    }
    return reached
}

func vertexDFS(g *graph.Graph, u *graph.Vertex, color map[*graph.Vertex]uint8) []*graph.Vertex {
    reached := make([]*graph.Vertex, 0)
    color[u] = grey
    for _, w := range g.GetAdj(u) {
        if color[w] == white {
            reached = append(reached, w)
            r := vertexDFS(g, w, color)
            reached = append(reached, r...)
        }
    }
    color[u] = black
    return reached
}

func buildGraph() graph.Graph {
    var g graph.Graph
    a := graph.Vertex{"A"}
    b := graph.Vertex{"B"}
    c := graph.Vertex{"C"}
    d := graph.Vertex{"D"}
    e := graph.Vertex{"E"}
    f := graph.Vertex{"F"}
    j := graph.Vertex{"J"}
    m := graph.Vertex{"M"}
    n := graph.Vertex{"N"}
    g.Vertices = append(g.Vertices, &a, &b, &c, &d, &e, &f, &j, &m, &n)
    g.AddBiEdge(&a, &c)
    g.AddBiEdge(&b, &c)
    g.AddBiEdge(&c, &d)
    g.AddBiEdge(&d, &e)
    g.AddBiEdge(&e, &f)
    g.AddBiEdge(&b, &m)
    g.AddBiEdge(&m, &n)
    g.AddEdge(&f, &c)
    return g
}

func printReachedVertices(reached []*graph.Vertex) {
    fmt.Println("Reached Vertices:")
    for i, v := range reached {
        if i > 0 {
            fmt.Printf(", ")
        }
        fmt.Printf("%v", v)
    }
    fmt.Println()
}

func main() {
    fmt.Println("Depth-First Search on Graph")
    g := buildGraph()
    fmt.Println(g)
    reached := DFS(&g)
    printReachedVertices(reached)
}