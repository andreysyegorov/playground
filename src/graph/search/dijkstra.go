package main

import (
    graph "graph/common"
    "fmt"
    "math"
)

func Dijkstra(g *graph.Graph, s *graph.Vertex) (map[*graph.Vertex]uint64, map[*graph.Vertex][]*graph.Vertex) {
    dist := make(map[*graph.Vertex]uint64)
    path := make(map[*graph.Vertex][]*graph.Vertex)
    path[s] = make([]*graph.Vertex, 0)
    visited := make(map[*graph.Vertex]bool)
    for _, v := range g.Vertices {
    	if v == s {
    		dist[v] = uint64(0)
    	} else {
    		dist[v] = uint64(math.Inf(1))
    	}
    	visited[v] = false
    }
    for {
    	var u *graph.Vertex
    	udist := uint64(math.Inf(1))
        for k, v := range visited {
        	if !v && dist[k] < udist {
        		u, udist = k, dist[k]
        	}
        }
        if u == nil {
        	break
        }
        for _, edge := range g.GetFromEdges(u) {
        	if !visited[edge.To] {
        		newdist := dist[u] + uint64(edge.Weight)
        		if newdist < dist[edge.To] {
        			dist[edge.To] = newdist
        			copy(path[edge.To], path[u])
        			path[edge.To] = append(path[edge.To], edge.To)
        		}
        	}
        }
        visited[u] = true
    }
    return dist, path
}

func buildGraph() graph.Graph {
    var g graph.Graph
    v1 := graph.Vertex{"1"}
    v2 := graph.Vertex{"2"}
    v3 := graph.Vertex{"3"}
    v4 := graph.Vertex{"4"}
    v5 := graph.Vertex{"5"}
    v6 := graph.Vertex{"6"}
    g.Vertices = append(g.Vertices, &v1, &v2, &v3, &v4, &v5, &v6)
    g.AddWeightedBiEdge(&v1, &v2, 7)
    g.AddWeightedBiEdge(&v1, &v3, 9)
    g.AddWeightedBiEdge(&v2, &v3, 10)
    g.AddWeightedBiEdge(&v1, &v6, 14)
    g.AddWeightedBiEdge(&v2, &v4, 15)
    g.AddWeightedBiEdge(&v3, &v4, 11)
    g.AddWeightedBiEdge(&v3, &v6, 2)
    g.AddWeightedBiEdge(&v4, &v5, 6)
    g.AddWeightedBiEdge(&v5, &v6, 9)
    return g
}

func printDistancesAndPaths(d map[*graph.Vertex]uint64, p map[*graph.Vertex][]*graph.Vertex) {
    fmt.Println("Distances and Paths:")
    for u, sd := range d {
        if sd == uint64(math.Inf(1)) {
            fmt.Printf("%v - Infinity\n", u.Value)
        } else {
            fmt.Printf("vertex: %v, path: %v, distance: %v\n", u.Value, p[u], sd)
        }
    }
}

func main() {
    fmt.Println("Dijkstra Search on Graph")
    g := buildGraph()
    start := g.GetVertex("1")
    fmt.Println(g)
    fmt.Println("Initial Vertex:", start)
    dist, path := Dijkstra(&g, start)
    printDistancesAndPaths(dist, path)
}
