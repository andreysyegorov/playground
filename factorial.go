package main

import (
	"fmt"
	"os"
	"strconv"
)

func factorial(num int) int {
	res := 1
	for ; num > 0; num-- {
		res *= num
	}
	return res
}

func main() {
	num, _ := strconv.Atoi(os.Args[1])
	fmt.Println("Factorial")
	fmt.Printf("%d! = %d\n", num, factorial(num))
}
