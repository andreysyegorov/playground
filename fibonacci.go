package main

import (
	"fmt"
	"os"
	"strconv"
)

func fibonacci(len int) []int {
	res := make([]int, len)
	res[0], res[1] = 0, 1
	for i := 2; i < len; i++ {
		res[i] = res[i - 2] + res[i - 1]
	}
	return res
}

func main() {
	num, _ := strconv.Atoi(os.Args[1])
	fmt.Println("Fibonacci sequence")
	fmt.Printf("First %d numbers: %d\n", num, fibonacci(num))
}
