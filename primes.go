package main

import (
    "fmt"
    "os"
    "strconv"
)

/*
Input: an integer n > 1.
 
 Let A be an array of Boolean values, indexed by integers 2 to n,
 initially all set to true.
 
 for i = 2, 3, 4, ..., not exceeding √n:
   if A[i] is true:
     for j = i^2, i^2+i, i^2+2i, i^2+3i, ..., not exceeding n:
       A[j] := false.
 
 Output: all i such that A[i] is true.
*/
func primeNumbers(n int) []int {
    n2 := n * n
    res := make([]int, n)
    a := make([]bool, n2)
    for i := 0; i < n2; i++ {
        a[i] = true
    }
    for i := 2; i < n2; i++ {
        if a[i] == true {
            for j := i*i; j < n2; j += i {
                a[j] = false
            }
        }
    }
    j := 0
    for i := 2; i < n2 && j < n; i++ {
        if a[i] == true {
            res[j] = i
            j++
        }
    }
    return res
}

func main() {
    len, _ := strconv.Atoi(os.Args[1])
    fmt.Println("Prime Numbers (Sieve of Eratosthenes)")
    fmt.Printf("First %d numbers: %v", len, primeNumbers(len))
}
