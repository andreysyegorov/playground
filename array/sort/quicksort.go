package main

import (
    "fmt"
    "math/rand"
    "os"
    "strconv"
    "time"
)

func QuickSort(arr []int, lo, hi int) {
    if lo < hi {
        p := partition(arr, lo, hi)
        QuickSort(arr, lo, p)
        QuickSort(arr, p+1, hi)
    }
}

func partition(arr []int, lo, hi int) int {
    pivot := arr[(lo+hi)/2]
    i, j := lo-1, hi+1
    for {
        for i++; arr[i] < pivot; i++ {
        }
        for j--; arr[j] > pivot; j-- {
        }
        if i >= j {
            return j
        }
        arr[i], arr[j] = arr[j], arr[i]
    }
}

func isSorted(arr []int) bool {
    for i := 1; i < len(arr); i++ {
        if arr[i - 1] > arr[i] {
            return false
        }
    }
    return true
}

func main() {
    arr := make([]int, len(os.Args) - 1)
    for i := 1; i < len(os.Args); i++ {
        arr[i - 1], _ = strconv.Atoi(os.Args[i])
    }
    if len(arr) == 1 {
        size := arr[0]
        arr = make([]int, size)
        for i := 0; i < size; i++ {
            arr[i] = rand.Intn(1000)
        }
    }
    fmt.Println("Quick Sort")
    start := time.Now()
    QuickSort(arr, 0, len(arr)-1)
    fmt.Printf("took %.2f seconds\n", time.Since(start).Seconds())
    fmt.Println("isSorted:", isSorted(arr))
}