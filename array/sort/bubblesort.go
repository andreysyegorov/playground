package main

import (
    "fmt"
    "os"
    "strconv"
)

func BubbleSort(arr []int) {
    for j := 0; j < len(arr) - 1; j++ {
        changed := false
        for i := 0; i < len(arr) - j - 1; i++ {
            if arr[i] > arr[i + 1] {
                arr[i], arr[i + 1] = arr[i + 1], arr[i]
                changed = true
            }
        }
        if !changed {
            break
        }
    }
}

func main() {
    arr := make([]int, len(os.Args) - 1)
    for i := 1; i < len(os.Args); i++ {
        arr[i - 1], _ = strconv.Atoi(os.Args[i])
    }
    fmt.Println("Bubble Sort")
    fmt.Println("Input:", arr)
    BubbleSort(arr)
    fmt.Println("Output:", arr)
}
