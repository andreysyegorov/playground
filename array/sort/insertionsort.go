package main

import (
    "fmt"
    "os"
    "strconv"
)

func InsertionSort(arr []int) {
    for i := 1; i < len(arr); i++ {
        ai := arr[i]
        j := i
        for j > 0 && ai < arr[j - 1] {
            arr[j] = arr[j - 1]
            j--
        }
        arr[j] = ai
    }
}

func main() {
    arr := make([]int, len(os.Args) - 1)
    for i := 1; i < len(os.Args); i++ {
        arr[i - 1], _ = strconv.Atoi(os.Args[i])
    }
    fmt.Println("Insertion Sort")
    fmt.Println("Input:", arr)
    InsertionSort(arr)
    fmt.Println("Output:", arr)
}