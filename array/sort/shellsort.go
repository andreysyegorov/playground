package main

import (
    "fmt"
    "math/rand"
    "os"
    "strconv"
    "time"
)

const DEBUG = false

func ShellSort(arr []int) {
    increment := len(arr)/2
    for increment >= 1 {
        for startIndex := 0; startIndex < increment; startIndex++ {
            insertionSort(arr, startIndex, increment)
        }
        increment = increment/2
    }
}

func insertionSort(arr []int, startIndex int, increment int) {
    for i := startIndex; i < len(arr)-1; i = i+increment {
        for j := min(i+increment, len(arr)-1); j >= increment; j = j-increment {
            if arr[j-increment] > arr[j] {
                arr[j], arr[j-increment] = arr[j-increment], arr[j]
            } else {
                break
            }
        }
    }
}

func min(a, b int) int {
    if a <= b {
        return a
    } else {
        return b
    }
}

func isSorted(arr[] int) bool {
    for i := 1; i < len(arr); i++ {
        if arr[i - 1] > arr[i] {
            return false
        }
    }
    return true
}

func main() {
    s, _ := strconv.Atoi(os.Args[1])
    arr := make([]int, s)
    for i := range arr {
        arr[i] = rand.Intn(1000)
    }
    fmt.Println("Shell Sort")
    if DEBUG {
        fmt.Println("Input:", arr)
    }
    start := time.Now()
    ShellSort(arr)
    fmt.Printf("took %.2f seconds\n", time.Since(start).Seconds())
    if DEBUG {
        fmt.Println("Output:", arr)
    }
    fmt.Println("isSorted:", isSorted(arr))
}
