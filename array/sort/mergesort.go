package main

import (
    "fmt"
    "os"
    "strconv"
)

func MergeSort(arr []int) []int {
    if len(arr) > 1 {
        arr1, arr2 := split(arr)
        return join(MergeSort(arr1), MergeSort(arr2))
    } else {
        return arr
    }
}

func join(arr1 []int, arr2 []int) []int {
    arr := make([]int, len(arr1) + len(arr2))
    iArr1, iArr2 := 0, 0
    for i := 0; i < len(arr); i++ {
        if iArr1 >= len(arr1) {
            arr[i] = arr2[iArr2]
            iArr2++
        } else if iArr2 >= len(arr2) {
            arr[i] = arr1[iArr1]
            iArr1++
        } else if arr1[iArr1] < arr2[iArr2] {
            arr[i] = arr1[iArr1]
            iArr1++
        } else {
            arr[i] = arr2[iArr2]
            iArr2++
        }
    }
    return arr
}

func split(arr []int) ([]int, []int) {
    i := len(arr)/2
    return arr[:i], arr[i:]
}

func main() {
    arr := make([]int, len(os.Args) - 1)
    for i := 1; i < len(os.Args); i++ {
        arr[i - 1], _ = strconv.Atoi(os.Args[i])
    }
    fmt.Println("Merge Sort")
    fmt.Println("Input:", arr)
    arr = MergeSort(arr)
    fmt.Println("Output:", arr)
}