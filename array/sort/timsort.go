package main

import (
    "fmt"
    "os"
    "math/rand"
    "strconv"
    "time"
)

const (
    MIN_GALLOP = 7
    DEBUG = false
)

type Run struct {
    start int
    length int
}

/*
TODOs:
  - refinement: adjust min gallop in mergeLo and mergeHi functions
*/

func TimSort(arr []int) {
    arrLen := len(arr)
    minGallop := MIN_GALLOP
    minRunLength := minrun(arrLen)
    if DEBUG {
        fmt.Println("minrun:", minRunLength)
        fmt.Println("min_gallop:", minGallop)
    }
    runs := make([]Run, 0)
    // add 2 elements in new run
    currentRunStart := 0
    currentRunLength := 2
    asc := arr[0] <= arr[1]
    i := 2
    for {
        if i >= arrLen {
            run := Run{start: currentRunStart, length: currentRunLength}
            if !asc {
                if DEBUG {
                    printRun("reverse desc run", arr, run)
                }
                reverseOrder(arr, currentRunStart, currentRunLength)
                if DEBUG {
                    printRun("add run", arr, run)
                }
            } else {
                if DEBUG {
                    printRun("add run [natural]", arr, run)
                }
            }
            runs = append(runs, run)
            break
        }
        if DEBUG {
            fmt.Println("-----")
            fmt.Println("i:", i)
        }
        if arr[i - 1] <= arr[i] { // asc
            if asc { // asc (same order)
                currentRunLength++
                i++
                if DEBUG {
                    run := Run{start: currentRunStart, length: currentRunLength}
                    printRun("current run [asc]", arr, run)
                }
                if i == arrLen {
                    run := Run{start: currentRunStart, length: currentRunLength}
                    runs = append(runs, run)
                    if DEBUG {
                        printRun("add run [natural]", arr, run)
                    }
                    break
                }
            } else { // desc (order changed)
                // random run - add elements till less than minrun
                if currentRunLength < minRunLength {
                    currentRunLength = min(arrLen - currentRunStart, minRunLength)
                    i = currentRunStart + currentRunLength
                    run := Run{start: currentRunStart, length: currentRunLength}
                    if DEBUG {
                        printRun("sort random run", arr, run)
                    }
                    binaryInsertionSort(arr, currentRunStart, i)
                    runs = append(runs, run)
                    if DEBUG {
                        printRun("add run", arr, run)
                    }
                    if i >= arrLen {
                        break
                    }
                } else {
                    // natural run with decreasing order - reverse
                    run := Run{start: currentRunStart, length: currentRunLength}
                    if DEBUG {
                        printRun("reverse desc run", arr, run)
                    }
                    reverseOrder(arr, currentRunStart, currentRunLength)
                    runs = append(runs, run)
                    if DEBUG {
                        printRun("add run", arr, run)
                    }
                    if i == arrLen - 1 { // last element - add new run of size 1 and exit
                        run := Run{start: i, length: 1}
                        runs = append(runs, run)
                        if DEBUG {
                            printRun("add run [single]", arr, run)
                        }
                        break
                    }
                }
                // add 2 elements in new run
                currentRunStart = i
                currentRunLength = 2
                asc = arr[i] <= arr[i + 1]
                i += 2
            }
        } else { // desc
            if asc == false { // desc (same order)
                currentRunLength++
                i++
                if DEBUG {
                    run := Run{start: currentRunStart, length: currentRunLength}
                    printRun("current run [desc]", arr, run)
                }
                if i == arrLen {
                    run := Run{start: currentRunStart, length: currentRunLength}
                    reverseOrder(arr, currentRunStart, currentRunLength)
                    runs = append(runs, run)
                    if DEBUG {
                        printRun("add run", arr, run)
                    }
                    break
                }
            } else { // asc (order changed)
                // random run - add elements till less than minrun
                if currentRunLength < minRunLength {
                    currentRunLength = min(arrLen - currentRunStart, minRunLength)
                    i = currentRunStart + currentRunLength
                    run := Run{start: currentRunStart, length: currentRunLength}
                    if DEBUG {
                        printRun("sort random run", arr, run)
                    }
                    binaryInsertionSort(arr, currentRunStart, i)
                    runs = append(runs, run)
                    if DEBUG {
                        printRun("add run", arr, run)
                    }
                    if i >= arrLen {
                        break
                    }
                } else {
                    // natural run
                    run := Run{start: currentRunStart, length: currentRunLength}
                    runs = append(runs, run)
                    if DEBUG {
                        printRun("add run [natural]", arr, run)
                    }
                    if i == arrLen - 1 { // last element - add new run of size 1 and exit
                        run := Run{start: i, length: 1}
                        runs = append(runs, run)
                        if DEBUG {
                            printRun("add run [single]", arr, run)
                        }
                        break
                    }
                }
                // add 2 elements in new run
                currentRunStart = i
                currentRunLength = 2
                asc = arr[i] <= arr[i + 1]
                i += 2
            }
        }
        runs = mergeCollapse(arr, runs, &minGallop)
    }
    if DEBUG {
        fmt.Println("-----")
        fmt.Println("all runs")
        for i, run := range(runs) {
            fmt.Printf("%d)\trun %v %v\n", i + 1, run, arr[run.start:run.start + run.length])
        }
        fmt.Println("-----")
    }
    merge(arr, runs, &minGallop)
}

/*
 * Merge if at least both of the following conditions/invariants are NOT satisfied:
 *   1. A > B+C
 *   2. B > C
 */
func mergeCollapse(arr []int, runs []Run, minGallop *int) []Run {
    s := len(runs)
    for s >= 3 {
        a, b, c := runs[s-3], runs[s-2], runs[s-1]
        if (a.length <= b.length + c.length) && (b.length <= c.length) {
            s -= 3
            if a.length < c.length {
                ab := mergeRuns(arr, a, b, minGallop)
                if DEBUG {
                    fmt.Printf("merge runs by invariants (A>B+C && B>C): A:%v, B: %v, C: %v, merge(A, B)\n", a, b, c)
                }
                s += 2
                runs[s - 2], runs[s - 1] = ab, c
            } else {
                bc := mergeRuns(arr, b, c, minGallop)
                if DEBUG {
                    fmt.Printf("merge runs by invariants (A>B+C && B>C): A:%v, B: %v, C: %v, merge(B, C)\n", a, b, c)
                }
                s += 2
                runs[s - 2], runs[s - 1] = a, bc
            }
        } else {
            break
        }
    }
    return runs[:s]
}

func merge(arr []int, runs []Run, minGallop *int) {
    s := len(runs)
    for s > 1 {
        run2 := runs[s - 1]
        s--
        run1 := runs[s - 1]
        s--
        run := mergeRuns(arr, run1, run2, minGallop)
        s++
        runs[s - 1] = run
        if DEBUG {
            printRun("result", arr, run)
            fmt.Println("runs", runs[:s])
            fmt.Println("-----")
        }
    }
}

func printRun(prefix string, arr []int, run Run) {
    fmt.Println(prefix, run, arr[run.start:run.start + run.length])
}

func minrun(n int) int {
    r := 0
    for n >= 64 {
        r |= n & 1
        n >>= 1
    }
    return n + r
}

func binaryInsertionSort(arr []int, start int, end int) {
    for i := start + 1; i < end; i++ {
        item := arr[i]
        j := binarySearch(arr, start, i - 1, item)
        for k := i; k > j; k-- {
            arr[k] = arr[k - 1]
        }
        arr[j] = item
    }
}

func binarySearch(arr []int, start int, end int, item int) int {
    for start < end {
        j := (end + start) / 2
        if item >= arr[j] {
            start = j + 1
        } else {
            end = j - 1
        }
    }
    if item < arr[start] {
        return start
    } else {
        return start + 1
    }
}

func reverseOrder(arr []int, start int, len int) {
    end := start + len - 1
    for i := 0; i < len / 2; i++ {
        arr[start + i], arr[end - i] = arr[end - i], arr[start + i]
    }
}

func mergeRuns(arr []int, run1 Run, run2 Run, minGallop *int) Run {
    start := binarySearch(arr, run1.start, run1.start + run1.length - 1, arr[run2.start])
    end := binarySearch(arr, run2.start, run2.start + run2.length - 1, arr[run1.start + run1.length - 1])
    if DEBUG {
        fmt.Println("merge_runs array", arr[run1.start:run2.start + run2.length])
        fmt.Printf("merge_runs: run1: %v, start: %d\n", run1, start)
        fmt.Printf("merge_runs: run2: %v, end: %d\n", run2, end)
    }
    run1WithOffset := Run{start: start, length: run1.length - start + run1.start}
    run2WithOffset := Run{start: run2.start, length: end - run2.start}
    if run1WithOffset.length <= run2WithOffset.length {
        mergeLo(arr, run1WithOffset.start, run2WithOffset.start, run2WithOffset.start + run2WithOffset.length, minGallop)
    } else {
        mergeHi(arr, run1WithOffset.start, run2WithOffset.start, run2WithOffset.start + run2WithOffset.length, minGallop)
    }
    return Run{start: run1.start, length: run1.length + run2.length}
}

// merge two sub-arrays of `arr` [low, bound) and [bound, high) with copying of former one
func mergeLo(arr []int, low int, bound int, high int, minGallop *int) {
    if DEBUG {
        fmt.Println("----- mergeLo START -----")
        fmt.Printf("arr: %v\nlow: %v\nbound: %v\nhigh: %v\n", arr, low, bound, high)
    }
    // `i0` - index of the first element in `arr`
    i0 := low
    // `arr1` - copy [i1, i2) from `arr`, `i1` - index of the first element in `arr1`
    arr1, i1 := make([]int, bound-low), 0
    copy(arr1, arr[low:bound])
    // `arr2` - reference to [bound, high) in `arr`, `i2` - index of the first element in `arr2`
    arr2, i2 := arr[bound:high], 0
    // `chosen1`/`chosen2` - number of consecutive time elements of `arr1`/`arr2` won (were copied to `arr`)
    chosen1, chosen2 := 0, 0
    // iterate over elements of `arr1` and `arr2` and put lesser one in `arr`
    if DEBUG {
        fmt.Printf("arr1: %v\narr2: %v\n", arr1, arr2)
    }
    for i1 < len(arr1) && i2 < len(arr2) {
        if chosen1 >= *minGallop { // galloping mode over `arr1`
            if DEBUG {
                fmt.Printf("gallop over %s from %s[%d]=%v looking for %s[%d]=%v\n", "arr1", "arr1", i1, arr1[i1], "arr2", i2, arr2[i2])
            }
            j1 := exponentialSearch(arr1, i1, len(arr1)-1, arr2[i2])
            // if galloped over at least `minGallop` elements of `arr1`
            if j1 - i1 - 1 >= *minGallop {
                // switch to galloping over `arr2`
                chosen1, chosen2 = 0, *minGallop
                if DEBUG {
                    fmt.Printf("%s won %d times - continue to gallop over %s\n", "arr1", j1 - i1 - 1, "arr2")
                }
            } else {
                chosen1, chosen2 = 0, 0
                if DEBUG {
                    fmt.Printf("%s won %d times - switch to 'one at a time' mode\n", "arr1", max(j1 - i1 - 1, 0))
                }
            }
            // copy galloped elements of `arr1` to `arr`
            for j1 > i1 {
                arr[i0] = arr1[i1]
                i0++
                i1++
            }
            // copy element of `arr2` searched in `arr1` to `arr`
            arr[i0] = arr2[i2]
            i0++
            i2++
        } else if chosen2 >= *minGallop { // galloping mode over `arr2`
            if DEBUG {
                fmt.Printf("gallop over %s from %s[%d]=%v looking for %s[%d]=%v\n", "arr2", "arr2", i2, arr2[i2], "arr1", i1, arr1[i1])
            }
            j2 := exponentialSearch(arr2, i2, len(arr2)-1, arr1[i1])
            // if galloped over at least `minGallop` elements of `arr2`
            if j2 - i2 - 1 >= *minGallop {
                // switch to galloping over `arr1`
                chosen1, chosen2 = *minGallop, 0
                if DEBUG {
                    fmt.Printf("%s won %d times - continue to gallop over %s\n", "arr2", j2 - i2 - 1, "arr1")
                }
            } else {
                chosen1, chosen2 = 0, 0
                if DEBUG {
                    fmt.Printf("%s won %d times - switch to 'one at a time' mode\n", "arr2", max(j2 - i2 - 1, 0))
                }
            }
            // copy galloped elements of `arr2` to `arr`
            for j2 > i2 {
                arr[i0] = arr2[i2]
                i0++
                i2++
            }
            // copy element of `arr1` searched in `arr2` to `arr`
            arr[i0] = arr1[i1]
            i0++
            i1++
        } else { // 'one at a time' mode
            if arr1[i1] <= arr2[i2] {
                arr[i0] = arr1[i1]
                i0++
                i1++
                chosen1++
                chosen2 = 0
            } else /* arr1[i1] > arr2[i2] */ {
                arr[i0] = arr2[i2]
                i0++
                i2++
                chosen1 = 0
                chosen2++
            }
            if DEBUG {
                if chosen1 >= *minGallop {
                    fmt.Printf("%s won %d times - switch to galloping mode\n", "arr1", chosen1)
                } else if chosen2 >= *minGallop {
                    fmt.Printf("%s won %d times - switch to galloping mode\n", "arr2", chosen2)
                }
            }
        }
    }
    // copy remaining elements of `arr1` into `arr`
    // (no need to copy `arr2` - all its elements already in `arr`)
    for i1 < len(arr1) {
        arr[i0] = arr1[i1]
        i0++
        i1++
    }
    if DEBUG {
        fmt.Printf("arr1+arr2: %v\n", arr[low:high])
        fmt.Println("----- mergeLo END -----")
    }
}

// merge two sub-arrays of `arr` [low, bound) and [bound, high) with copying of latter one
func mergeHi(arr []int, low int, bound int, high int, minGallop *int) {
    if DEBUG {
        fmt.Println("----- mergeHi START -----")
        fmt.Printf("arr: %v\nlow: %v\nbound: %v\nhigh: %v\n", arr, low, bound, high)
    }
    // `i0` - index of this last element in `arr`
    i0 := high-1
    // `arr1` - reference to [i1, i2) in `arr`, `i1` - index of the last element in `arr1`
    arr1, i1 := arr[low:bound], bound-low-1
    // `arr2` - copy [bound, high) from `arr`, `i2` - index of the last element in `arr2`
    arr2, i2 := make([]int, high-bound), high-bound-1
    copy(arr2, arr[bound:high])
    // `chosen1`/`chosen2` - number of consecutive time elements of `arr1`/`arr2` won (were copied to `arr`)
    chosen1, chosen2 := 0, 0
    // iterate over elements of `arr1` and `arr2` and put greater one in `arr`
    if DEBUG {
        fmt.Printf("arr1: %v\narr2: %v\n", arr1, arr2)
    }
    for i1 >= 0 && i2 >= 0 {
        if chosen1 >= *minGallop { // galloping mode over `arr1`
            if DEBUG {
                fmt.Printf("gallop over %s from %s[%d]=%v looking for %s[%d]=%v\n", "arr1", "arr1", i1, arr1[i1], "arr2", i2, arr2[i2])
            }
            j1 := exponentialSearch(arr1, 0, i1, arr2[i2])
            // if galloped over at least `minGallop` elements of `arr1`
            if i1 - j1 - 1 >= *minGallop {
                // switch to galloping over `arr2`
                chosen1, chosen2 = 0, *minGallop
                if DEBUG {
                    fmt.Printf("%s won %d times - continue to gallop over %s\n", "arr1", i1 - j1 - 1, "arr2")
                }
            } else {
                chosen1, chosen2 = 0, 0
                if DEBUG {
                    fmt.Printf("%s won %d times - switch to 'one at a time' mode\n", "arr1", max(i1 - j1 - 1, 0))
                }
            }
            // copy galloped elements of `arr1` to `arr`
            for j1 <= i1 {
                arr[i0] = arr1[i1]
                i0--
                i1--
            }
            // copy element of `arr2` searched in `arr1` to `arr`
            arr[i0] = arr2[i2]
            i0--
            i2--
        } else if chosen2 >= *minGallop { // galloping mode over `arr2`
            if DEBUG {
                fmt.Printf("gallop over %s from %s[%d]=%v looking for %s[%d]=%v\n", "arr2", "arr2", i2, arr2[i2], "arr1", i1, arr1[i1])
            }
            j2 := exponentialSearch(arr2, 0, i2, arr1[i1])
            // if galloped over at least `minGallop` elements of `arr2`
            if i2 - j2 - 1 >= *minGallop {
                // switch to galloping over `arr1`
                chosen1, chosen2 = *minGallop, 0
                if DEBUG {
                    fmt.Printf("%s won %d times - continue to gallop over %s\n", "arr2", i2 - j2 - 1, "arr1")
                }
            } else {
                chosen1, chosen2 = 0, 0
                if DEBUG {
                    fmt.Printf("%s won %d times - switch to 'one at a time' mode\n", "arr2", max(i2 - j2 - 1, 0))
                }
            }
            // copy galloped elements of `arr2` to `arr`
            for j2 <= i2 {
                arr[i0] = arr2[i2]
                i0--
                i2--
            }
            // copy element of `arr1` searched in `arr2` to `arr`
            arr[i0] = arr1[i1]
            i0--
            i1--
        } else { // 'one at a time' mode
            if arr1[i1] > arr2[i2] {
                arr[i0] = arr1[i1]
                i0--
                i1--
                chosen1++
                chosen2 = 0
            } else /* arr1[i1] <= arr2[i2] */ {
                arr[i0] = arr2[i2]
                i0--
                i2--
                chosen1 = 0
                chosen2++
            }
            if DEBUG {
                if chosen1 >= *minGallop {
                    fmt.Printf("%s won %d times - switch to galloping mode\n", "arr1", chosen1)
                } else if chosen2 >= *minGallop {
                    fmt.Printf("%s won %d times - switch to galloping mode\n", "arr2", chosen2)
                }
            }
        }
    }
    // copy remaining elements of `arr2` into `arr`
    // (no need to copy `arr1` - all its elements already in `arr`)
    for i2 >= 0 {
        arr[i0] = arr2[i2]
        i0--
        i2--
    }
    if DEBUG {
        fmt.Printf("arr1+arr2: %v\n", arr[low:high])
        fmt.Println("----- mergeHi END -----")
    }
}

func exponentialSearch(arr []int, start int, end int, item int) int {
    bound := 1
    for bound + start - 1 < end + 1 && arr[bound + start - 1] < item {
        bound *= 2
    }
    return binarySearch(arr, max(bound / 2 + start - 1, start), min(bound + start, end), item)
}

func min(a, b int) int {
    if a <= b {
        return a
    } else {
        return b
    }
}

func max(a, b int) int {
    if a >= b {
        return a
    } else {
        return b
    }
}

func abs(a int) int {
    if a >= 0 {
        return a
    } else {
        return -a
    }
}

func isSorted(arr []int) (bool, int, []int) {
    for i := 1; i < len(arr); i++ {
        if arr[i - 1] > arr[i] {
            return false, i, arr[i-1:i+1]
        }
    }
    return true, -1, nil
}

func main() {
    arr := make([]int, len(os.Args) - 1)
    for i := 1; i < len(os.Args); i++ {
        arr[i - 1], _ = strconv.Atoi(os.Args[i])
    }
    if len(arr) == 1 {
        size := arr[0]
        arr = make([]int, size)
        for i := 0; i < size; i++ {
            arr[i] = rand.Intn(1000)
        }
    }
    fmt.Println("Tim Sort")
    if DEBUG {
        fmt.Println("Input:", arr)
    }
    start := time.Now()
    TimSort(arr)
    fmt.Printf("took %.2f seconds\n", time.Since(start).Seconds())
    if DEBUG {
        fmt.Println("Output:", arr)
    }
    sorted, i, fragment := isSorted(arr)
    if sorted {
        fmt.Println("Sorted: YES")
    } else {
        fmt.Println("Sorted: NO")
        fmt.Printf("Problem area: offset=%d, fragment=%v\n", i, fragment)
    }
}
