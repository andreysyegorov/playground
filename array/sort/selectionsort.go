package main

import (
    "fmt"
    "os"
    "strconv"
)

func SelectionSort(arr []int) {
    for i := 0; i < len(arr) - 1; i++ {
        jmin:= i
        for j := i + 1; j < len(arr); j++ {
            if arr[j] < arr[jmin] {
                jmin = j
            }
        }
        if jmin != i {
            arr[i], arr[jmin] = arr[jmin], arr[i]
        }
    }
}

func main() {
    arr := make([]int, len(os.Args) - 1)
    for i := 1; i < len(os.Args); i++ {
        arr[i - 1], _ = strconv.Atoi(os.Args[i])
    }
    fmt.Println("Selection Sort")
    fmt.Println("Input:", arr)
    SelectionSort(arr)
    fmt.Println("Output:", arr)
}