package main

import (
    "fmt"
    "os"
    "strconv"
)

func BinaryInsertionSort(arr []int) {
    for i := 1; i < len(arr); i++ {
        item := arr[i]
        j := binarySearch(arr, 0, i - 1, item)
        for k := i; k > j; k-- {
            arr[k] = arr[k - 1]
        }
        arr[j] = item
    }
}

func binarySearch(arr []int, start int, end int, item int) int {
    for start < end {
        j := (end + start) / 2
        if item >= arr[j] {
            start = j + 1
        } else {
            end = j - 1
        }
    }
    if item < arr[start] {
        return start
    } else {
        return start + 1
    }
}

func main() {
    arr := make([]int, len(os.Args) - 1)
    for i := 1; i < len(os.Args); i++ {
        arr[i - 1], _ = strconv.Atoi(os.Args[i])
    }
    fmt.Println("Binary Insertion Sort")
    fmt.Println("Input:", arr)
    BinaryInsertionSort(arr)
    fmt.Println("Output:", arr)
}
