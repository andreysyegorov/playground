package main

import (
    "fmt"
    "os"
    "math/rand"
    "strconv"
    "time"
)

const (
    DEBUG = false
)

func HeapSort(arr []int) {
    buildMaxHeap(arr)
    if DEBUG {
        fmt.Println("Max Heap", arr)
        fmt.Println("-----")
    }
    for end := len(arr) - 1; end > 0; {
        arr[0], arr[end] = arr[end], arr[0]
        end--
        if DEBUG {
            fmt.Println("heap [before sift]", arr[0:end+1])
        }
        siftDown(arr, 0, end)
        if DEBUG {
            fmt.Println("heap [after sift]", arr[0:end+1])
            fmt.Println("sorted", arr[end+1:])
            fmt.Println("-----")
        }
    }
}

func buildMaxHeap(arr []int) {
    for i := 1; i < len(arr); i++ {
        for cur, par := i, (i-1)/2; par >= 0; par = (cur-1)/2 {
            if arr[cur] > arr[par] {
                arr[cur], arr[par] = arr[par], arr[cur]
                cur = par
            } else {
                break
            }
        }
    }
}

func siftDown(arr []int, start, end int) {
    currentNode := start
    for currentNode < end {
        leftChild := 2*currentNode+1
        rightChild := leftChild+1
        if rightChild <= end {
            if arr[currentNode] < arr[leftChild] {
                if arr[leftChild] < arr[rightChild] {
                    arr[currentNode], arr[rightChild] = arr[rightChild], arr[currentNode]
                    currentNode = rightChild
                } else {
                    arr[currentNode], arr[leftChild] = arr[leftChild], arr[currentNode]
                    currentNode = leftChild
                }
            } else if arr[currentNode] < arr[rightChild] {
                arr[currentNode], arr[rightChild] = arr[rightChild], arr[currentNode]
                currentNode = rightChild
            } else {
                break
            }
        } else {
            if leftChild <= end && arr[currentNode] < arr[leftChild] {
                arr[currentNode], arr[leftChild] = arr[leftChild], arr[currentNode]
                currentNode = leftChild
            } else {
                break
            }
        }
    }
}

func isSorted(arr []int) bool {
    for i := 1; i < len(arr); i++ {
        if arr[i] < arr[i-1] {
            return false
        }
    }
    return true
}

func main() {
    s, _ := strconv.Atoi(os.Args[1])
    arr := make([]int, s)
    for i := range arr {
        arr[i] = rand.Intn(1000)
    }
    fmt.Println("Heap Sort")
    if DEBUG {
        fmt.Println("Input:", arr)
        fmt.Println("isSorted:", isSorted(arr))
    }
    start := time.Now()
    HeapSort(arr)
    if DEBUG {
        fmt.Println("Output:", arr)
    }
    fmt.Printf("took %.2f seconds\n", time.Since(start).Seconds())
    fmt.Println("isSorted:", isSorted(arr))
}
