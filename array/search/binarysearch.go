package main

import (
	"fmt"
	"os"
	"strconv"
)

func BinarySearch(arr []int, item int) int {
	start := 0
	end := len(arr) - 1
	for {
		if start > end {
			return -1
		}
		i := (end + start) / 2
		if item < arr[i] {
			end = i - 1
		} else if item > arr[i] {
			start = i + 1
		} else {
			return i
		}
	}
}

func main() {
	arr := make([]int, len(os.Args) - 2)
	item, _ := strconv.Atoi(os.Args[1])
    for i := 0; i < len(os.Args) - 2; i++ {
        arr[i], _ = strconv.Atoi(os.Args[i + 2])
    }
    fmt.Println("Binary Search")
    fmt.Println("Input Array:", arr)
    fmt.Println("Input Searching Item:", item)
    index := BinarySearch(arr, item)
    fmt.Println("Output Searching Item Index:", index)
}