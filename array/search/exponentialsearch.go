package main

import (
	"fmt"
	"os"
	"strconv"
)

func ExponentialSearch(arr []int, item int) int {
	bound := 1
	for bound < len(arr) && arr[bound] < item {
		bound *= 2
	}
	return binarysearch(arr, item, bound / 2, min(bound + 1, len(arr) - 1))
}

func binarysearch(arr []int, item int, start int, end int) int {
	for {
		if start > end {
			return -1
		}
		i := (end + start) / 2
		if item < arr[i] {
			end = i - 1
		} else if item > arr[i] {
			start = i + 1
		} else {
			return i
		}
	}
}

func min(a, b int) int {
	if a <= b {
		return a
	} else {
		return b
	}
}

func main() {
	item, _ := strconv.Atoi(os.Args[1])
	arr := make([]int, len(os.Args) - 2)
	for i, a := range(os.Args[2:]) {
		arr[i], _ = strconv.Atoi(a)
	}
	fmt.Println("Exponential Search")
	fmt.Println("[Input] Item:", item)
	fmt.Println("[Input] Array:", arr)
	index := ExponentialSearch(arr, item)
	fmt.Println("[Output] Item Index:", index)
}